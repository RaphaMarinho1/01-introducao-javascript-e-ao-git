for (var i = 0; i < 10; i++) {
    var p = document.createElement("li");
    p.innerHTML = "Hello World";
    p.setAttribute("id", "hw"+i);
    document.body.appendChild(p);
 }

//Hello World 2
document.getElementById("hw1").style.textTransform = "uppercase";

//Hello World 3
document.getElementById("hw2").style.textTransform = "lowercase";

//Hello World 4
document.getElementById("hw3").style.opacity = "0.3";

//Hello World 5
document.getElementById("hw4").style.filter = "blur(1px)";

//Hello World 6
document.getElementById("hw5").style.fontStyle = "italic";

//Hello World 7
hw6Text = 'Hello World'.split('').reverse().join('');
document.getElementById("hw6").innerHTML = hw6Text;

//Hello World 8
hw7Text = 'Hello World'.split('').join('<br>');
document.getElementById("hw7").innerHTML = hw7Text;

//Hello World 9
hw8Text = 'Hello World'.split('').join(' ');
document.getElementById("hw8").innerHTML = hw8Text;

//Hello World 10
document.getElementById("hw9").style.writingMode = "vertical-rl";



